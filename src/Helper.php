<?php

namespace Magraine\Benchmark;

use Faker\Factory;
use Faker\Generator;

class Helper {
    public static function percent(float $a, float $b): string
    {
        $percent = round(($b - $a) / abs($a) * 100, 2);
        if ($percent >= 0.0) {
            return '+' . $percent . '%';
        }

        return $percent . '%';
    }

    public static function times(float $a, float $b): string
    {
        $times = round($b / $a, 2);
        if ($times >= 1) { 
            return '×' . $times;
        } 

        return '÷' . round($a / $b, 2);
    }
}