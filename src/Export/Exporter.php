<?php

namespace Magraine\Benchmark\Export;

class Exporter {

    /** Export with short array (but always keys) */
    public static function var_export_replace_with_short_array(array $data): string {
        $data = var_export($data, true);
        $data = preg_replace(
            ["#array\s\($#m", "#\),$#m"],
            ['[', '],'],
            $data
        );
        $data = substr($data, 0, -1) . ']';
        return $data;
    }


    /** Export with short array and no keys if list */
    public static function export_short_array_no_keys_if_list(array $data, $indent = 0): string {
        $_indent = str_repeat("\t", $indent);
        $export =  $_indent . "[\n";
        $keys = !array_is_list($data);
        foreach ($data as $key => $value) {
            if ($keys) {
                $export .= $_indent . "\t'" . addslashes($key) . "' => ";
            }
            if (is_array($value)) {
                $export .= self::export_short_array_no_keys_if_list($value, $indent + 1);
            } else {
                $export .= $_indent  . ($keys ? '' : "\t") . var_export($value, true);
            }
            $export .= ",\n";
        }
        $export .=  $_indent . ']';
        return $export;
    }
}