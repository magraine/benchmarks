# Some Benchmarks

## installation

```
composer install
```

## stringify

Test serialize vs json_encode

```
php benchmark/stringify.php
```

### Result

(on local MBP Pro M2)

With `string[]` as source :

- `serialize` equals `json_encode` on small data
- `serialize` is fastest than `json_encode` as data length growths (up to ~ 92% !)
- `json_encode` produces ~ 20% to 25% less length string result than `serialize`


## hash

Test to get an hash from an array.
Uses of `md5`, `crc32`, `xxh3`, `xxh128`

- `md5` & `xxh128` are 128 bits hashes
- `xxh3` is 64 bits hash
- `crc32` is 32 bits hash

See also: https://php.watch/articles/php-hash-benchmark

```
php benchmark/hash.php
```

### Result

#### Note 

- Result differ on `hash('crc32*')` functions compared to `crc32()`.
  - first is really slower on macos M2 php 8.2.4, but not with big difference on Debian 10 php 8.2.8


#### Analysis

With `string[]` as source :

- `md5` is always the slowest (by far as data volume increase)
- `crc32` is always the fastest (~ ×30 times faster than `md5` on big data volumes)
- `xxh128` & `xxh3` are in beetween (respectively ~ × 5.6 and 6 times faster than `md5`, ~ 5 times slower than `crc32`)

If we also count the time of the array to string conversion

- couple `serialize + crc32` is always the fastest
- couple `json_encode + md5` is fastest than `serialize + md5` (on macos M2 php 8.2.4 but not on a debian php 8.2.8)

If we want a 128 bits hash (like md5)

- couple `serialize + xxh128` is the fastest (~3.75 times faster than md5)

If we want a 64 bits hash

- couple `serialize + xxh3` is the fastest (~3.75 times faster than md5)


## export

Test to export data in file (for PHP), write & read
With `serialize`, `json_encode`, `var_export`, `Symfony/VarExport`

Additionnaly 2 more functions are tested
- `var_export_replace_with_short_array` (replace `array()` to `[]` after `var_export` function)
- `export_short_array_no_keys_if_list` (sowehow like Symfony VarExporter but not for objects; short array and no keys if an array is a list)

```
php benchmark/export.php
```

### Result

(on local MBP Pro M2)

#### Generate export

On big data (10000 Faker sentences)

- `serialize` is the fastest
- `json_encode` is ×2 times slower than `serialize`
- `var_export` is ×5 times slower than `serialize`
- `Symfony VarExporter` is ×30 slower than `serialize` (~ ×6.3 times slower than `var_export`)
- `var_export_replace_with_short_array` is like `var_export`
- `export_short_array_no_keys_if_list` is ×2.3 slower than `var_export` (~ ×2.5 times faster than Symfony VarExporter)

Symfony VarExporter is slow if we compare with other solutions for arrays.

- If we don’t have objects (and want php), `var_export` is probably more appropriate
- If we want a pretty print of arrays, maybe `export_short_array_no_keys_if_list` si a thing to consider
  due to the low speed of Symfony VarExporter for that
- If Symfony VarExporter is only use for cache warmup from CLI, it’s certainly good also (with pretty print array).

#### Reading data from files cache

##### Without opcache

- Reading and decoding `serialize` is the fastest
- `json_decode` is ×2.5 slower than `serialize`
- `Symfony VarExporter` & `export_short_array_no_keys_if_list` are ×3.8 slower than `serialize`
- `var_export` & `var_export_replace_with_short_array` are ×4.8 slower than `serialize`

##### With opcache

- All php content files are the fastest
- `serialize` & `json_decode` don’t change, but are ×13 times slower than include `php files` !

To conclude : 

- reading data from PHP files that goes to opcache is really fast,
- but it needs more resource to be exported
