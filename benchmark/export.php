<?php
use Faker\Factory;
use Magraine\Benchmark\Export\Exporter;
use Magraine\Benchmark\Helper;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\VarExporter\VarExporter;

error_reporting(E_ALL);
@ini_set("display_errors", 1);

require dirname(__DIR__) . '/vendor/autoload.php';

use DragonCode\Benchmark\Benchmark;

$faker = Factory::create('fr_FR');
$io = new SymfonyStyle(new ArgvInput(), new ConsoleOutput());

$io->title('Benchmark exports');
$io->listing([
    'var_export', 
    'serialize', 
    'json_encode', 
    'symfony/var-exporter', 
    'Local::var_export_replace_with_short_array', 
    'Local::export_short_array_no_keys_if_list', 
]);

foreach ([
	'short' => ['sentences' => 3, 'iterations' => 5000],
	'medium' => ['sentences' => 100, 'iterations' => 1000],
	'long' => ['sentences' => 10000, 'iterations' => 100],
] as $name => [
	'sentences' => $sentences,
	'iterations' => $iterations,
]) {
    $data = $faker->sentences($sentences);
	$io->section('Test ' . $name);
	$io->text('- ' . $sentences . ' sentences');
	$io->text('- ' . $iterations . ' iterations');
	$io->writeln('');

    Benchmark::start()
        ->iterations(10)
        ->withoutData()
        ->round(1)
        ->compare([
        'var_export' => function()  use ($data) {
            var_export($data, true);
        },
        'var_export_replace_with_short_array' => function()  use ($data) {
            Exporter::var_export_replace_with_short_array($data);
        },
        'export_short_array_no_keys_if_list' => function()  use ($data) {
            Exporter::export_short_array_no_keys_if_list($data);
        },
        'serialize' => function()  use ($data) {
            serialize($data);
        },
        'json_encode' => function()  use ($data) {
            json_encode($data);
        },
        'Symfony' => function()  use ($data) {
            VarExporter::export($data);
        },
    ]);
}

$io->title('Benchmark read opcache files of exports');

$sentences = 1000;
$data = $faker->sentences($sentences);
$io->text('- ' . $sentences . ' sentences');

function export_tmp_filename(string $name): string {
    return sys_get_temp_dir() . '/bench_export_' . $name . '.php';
}

$io->section("Prepare Export data files");

$list = [
    'var_export' => '<?php return ' . var_export($data, true) . ';',
    'var_export_replace_with_short_array' => '<?php return ' . Exporter::var_export_replace_with_short_array($data) . ';',
    'export_short_array_no_keys_if_list' => '<?php return ' . Exporter::export_short_array_no_keys_if_list($data) . ';',
    'serialize' => serialize($data),
    'json_encode' => json_encode($data),
    'symfony' => '<?php return ' . VarExporter::export($data) . ';',
];
foreach ($list as $name => $content) {
    $file = export_tmp_filename($name);
    opcache_invalidate($file);
    $io->write('- write: ' . $file . ' ');
    if (file_put_contents($file, $content)) {
        $io->write('(✅ write) ');
    } else {
        $io->write('(❌ write) ');
    }
    // need time less than start of this script to be in opcache compile 
    # touch($file, time() - 3600); 
    # opcache_compile_file($file);
    if (opcache_is_script_cached($file)) {
        $io->write('(✅ opcache) ');
    } else {
        $io->write('(❌ opcache) ');
    }
    $io->write('(' . filesize($file) . ' octets)');
    $io->writeln('');
}

foreach ([
    'Read files without opcache',
    'Read files with opcache',
] as $key => $title) {

    $io->section($title);

    if ($key === 1) {
        foreach ($list as $name => $content) {
            $file = export_tmp_filename($name);
            $io->write('- opcache: ' . $file . ' ');
            // need time less than start of this script to be in opcache compile 
            touch($file, time() - 3600); 
            opcache_compile_file($file);
            if (opcache_is_script_cached($file)) {
                $io->write('✅ ');
            } else {
                $io->write('❌');
            }
            $io->writeln('');
        }
        $io->writeln('');
    }

    Benchmark::start()
        ->iterations(1000)
        ->withoutData()
        ->round(1)
        ->compare([
        'var_export' => function() {
            $file = export_tmp_filename('var_export');
            $x = include $file;
        },
        'var_export_replace_with_short_array' => function() {
            $file = export_tmp_filename('var_export_replace_with_short_array');
            $x = include $file;
        },
        'export_short_array_no_keys_if_list' => function() {
            $file = export_tmp_filename('export_short_array_no_keys_if_list');
            $x = include $file;
        },
        'serialize' => function()  {
            $file = export_tmp_filename('serialize');
            $x = file_get_contents($file);
            $x = unserialize($x);
        },
        'json_encode' => function() {
            $file = export_tmp_filename('json_encode');
            $x = file_get_contents($file);
            $x = json_decode($x);
        },
        'symfony' => function() {
            $file = export_tmp_filename('symfony');
            $x = include $file;
        },
    ]);

}