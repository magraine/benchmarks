<?php
use Faker\Factory;
use Magraine\Benchmark\Helper;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Style\SymfonyStyle;

error_reporting(E_ALL);
@ini_set("display_errors", 1);

require dirname(__DIR__) . '/vendor/autoload.php';

use DragonCode\Benchmark\Benchmark;

$faker = Factory::create('fr_FR');
$io = new SymfonyStyle(new ArgvInput(), new ConsoleOutput());

$io->title('Benchmark serialize / json_encode');

foreach ([
	'short' => ['sentences' => 3, 'iterations' => 10000],
	'medium' => ['sentences' => 100, 'iterations' => 1000],
	'long' => ['sentences' => 10000, 'iterations' => 100],
	'extralong' => ['sentences' => 100000, 'iterations' => 10],
] as $name => [
	'sentences' => $sentences,
	'iterations' => $iterations,
]) {
	$data = $faker->sentences($sentences);
	$io->section('Test ' . $name);
	$io->text('- ' . $sentences . ' sentences');
	$io->text('- ' . $iterations . ' iterations');
	$io->writeln('');

	$len_serialize = strlen(serialize($data));
	$len_json_encode = strlen(json_encode($data));

	$io->text('- serialize length: ' . $len_serialize);
	$io->text('- json_encode length: ' . $len_json_encode);
	$io->text('- diff length: ' . Helper::percent($len_serialize, $len_json_encode));
	$io->writeln('');

	Benchmark::start()
		->iterations($iterations)
		->withoutData()
		->round(1)
		->compare([
		'serialize' => function()  use ($data) {
			serialize($data);
		},
		'json_encode' => function()  use ($data) {
			json_encode($data);
		},
	]);
}

