<?php

namespace Magraine\Benchmark\Test;

use Generator;
use Magraine\Benchmark\Helper;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;


class HelperTest extends TestCase {
    #[DataProvider('providerPercent')]
    public function testPercent($expected, ...$args) {
        $this->assertSame($expected, Helper::percent(...$args));
    }

    public static function providerPercent(): Generator {
        yield 'zero' => ['+0%', 10, 10];
        yield 'up' => ['+900%', 10, 100];
        yield 'down' => ['-90%', 100, 10];
        yield 'float up' => ['+23.46%', 100, 123.4567];
        yield 'float down' => ['-1.23%', 100, 98.7654];
    }

    #[DataProvider('providerTimes')]
    public function testTimes($expected, ...$args) {
        $this->assertSame($expected, Helper::times(...$args));
    }

    public static function providerTimes(): Generator {
        yield 'zero' => ['×1', 10, 10];
        yield 'up' => ['×2', 10, 20];
        yield 'down' => ['÷2', 20, 10];
        yield 'float up' => ['×1.23', 100, 123.4567];
        yield 'float down' => ['÷1.01', 100, 98.7654];
    }
}