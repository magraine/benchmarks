<?php

namespace Magraine\Benchmark\Test;

use Generator;
use Magraine\Benchmark\Export\Exporter;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;


class ExporterTest extends TestCase {

    #[DataProvider('providerExports')]
    public function testVarExportReplaceShortArray($expected) {
        $export = Exporter::var_export_replace_with_short_array($expected);
        $this->assertIsString($export);
        $actual = eval('return ' . $export . ';');
        $this->assertSame($expected, $actual);
    }

    #[DataProvider('providerExports')]
    public function testExportShortArrayNoKeysIfList($expected) {
        $export = Exporter::export_short_array_no_keys_if_list($expected);
        $this->assertIsString($export);
        $actual = eval('return ' . $export . ';');
        $this->assertSame($expected, $actual);
    }

    public static function providerExports(): Generator {
        yield 'simple' => [
            [
                'int' => 10,
                'float' => 12.30,
                'null' => null,
                'string' => 'a string',
                'string with array start' => 'array(',
                'string with array end' => ')',
                'string with array end comma' => '),',
                'string with short array start' => '[',
                'string with short array end' => ']',
                'string with short array end comma' => '],',
            ]
        ];
        yield 'list' => [
            [
                10,
                12.30,
                null,
                'a string',
                'array(',
                ')',
                '),',
                '[',
                ']',
                '],',
            ]
        ];
        yield '0 keys list' => [
            [
                0 => 0, 
                1 => 1, 
                2 => 2, 
                3 => 3, 
                4 => 4
            ]
        ];
        yield 'depth' => [
            [
                'list' => [0, 1, 2, 3, 4],
                'keys' => [10 => 10, 20 => 20, 30 => 30, 40 => 40, 50 => 50],
                '0 keys list' => [0 => 0, 1 => 1, 2 => 2, 3 => 3, 4 => 4],
            ]
        ];
    }
}